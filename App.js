/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {View, Text} from 'react-native';
import WifiManager from 'react-native-wifi-reborn';
import {PermissionsAndroid} from 'react-native';
import RNUpnp from 'react-native-upnp';

const App: () => React$Node = () => {
  getWifiPermission();
  WifiManager.connectionStatus(isConnected => {
    if (isConnected) {
      console.log('is connected');
    } else {
      console.log('is not connected');
    }
  });
  WifiManager.connectToProtectedSSID('iPhone', '99997777', false).then(
    () => {
      console.log('Connected successfully!');
    },
    () => {
      console.log('Connection failed!');
    },
  );

  WifiManager.getCurrentWifiSSID().then(
    ssid => {
      console.log('Your current connected wifi SSID is ' + ssid);
    },
    () => {
      console.log('Cannot get current SSID!');
    },
  );

  // WifiManager.loadWifiList(
  //   wifiStringList => {
  //     var wifiArray = JSON.parse(wifiStringList);
  //     console.log(wifiArray);
  //   },
  //   error => {
  //     console.log(error);
  //   },
  // );

  return (
    <>
      <View>
        <Text>Hello</Text>
      </View>
    </>
  );
};

export default App;

async function getWifiPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: 'Wifi networks',
        message: 'We need your permission in order to find wifi networks',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log('Thank you for your permission! :)');
    } else {
      console.log('You will not able to retrieve wifi available networks list');
    }
  } catch (err) {
    console.warn(err);
  }
}
